return {
	"christoomey/vim-tmux-navigation",
	vim.keymap.set("n", "C-h", ":TmuxNavigateLeft<CR>"),
	vim.keymap.set("n", "C-j", ":TmuxNavigateDown<CR>"),
	vim.keymap.set("n", "C-k", ":TmuxNavigateUp<CR>"),
	vim.keymap.set("n", "C-j", ":TmuxNavigateRight<CR>"),
}
